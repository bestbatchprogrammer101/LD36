﻿/*
 *    @Scryptonite
 *
 * Just attach this component to any GameObject with a Collider.
 *
 * If you end up using my cursor set, the hotspot is pretty good at <23, 26>.
 *
 * You might want to update the Texture Type of whatever cursor you use to "Cursor".
 *
 * Don't forget to update the default cursor and hotspot under Edit/Project Settings/Player.
 *
*/

using UnityEngine;

public class ChangeCursor : MonoBehaviour {
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotspot = Vector2.zero;
    public Texture2D mouseOverCursor;

    void OnMouseEnter() {
        Cursor.SetCursor(mouseOverCursor, hotspot, cursorMode);
    }

    void OnMouseExit() {
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
    }
}