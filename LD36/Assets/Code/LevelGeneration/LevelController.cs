﻿using UnityEngine;
using System.Collections.Generic;

public class LevelController : MonoBehaviour
{
    public int seed = 1;
    //public bool usePerlin = false;
    //public bool useBoth = true;
    public bool deleteOffScreenTiles = false;
    public int overRenderX = 3; //Renders this many tiles past the edge of the screen. This allows for generating tiles without the player catching it.
    public int overRenderY = 5;
    public float scale = 1000; //only used for perlin
    public List<BiomeController> biomes = new List<BiomeController>();//prefabs to spawn as ground.
    List<CMTile> tilesToGenerate = new List<CMTile>();
    List<CMTile> generatedTiles = new List<CMTile>();
    GameObject tileContainer;
    public int needToCreateCount = 0;
    public float deltaTime;
    public CMBounds screenBounds = new CMBounds();
    [Range(0,1)]
    //public float mergeStrength = .5f;

    float lastLeft, lastRight, lastTop, lastBottom;

    int chunkOffset = 1000;
    int chunkSize = 20;
    GameObject[,] chunks = new GameObject[2000, 2000];
    public GameObject[] objectsToTrack;

    public List<GameObject> liveChunks = new List<GameObject>();

    void Start()
    {
        UpdateBounds();

        tileContainer = new GameObject();
        tileContainer.name = "TileContainer";
    }
    bool firstLoop = false;

    void Update()
    {
        //UpdateBounds();
        List<Vector2> chunksToMakeLive = new List<Vector2>();
        for (int i = 0; i < objectsToTrack.Length; i++)
        {
            if (objectsToTrack[i].GetComponent<Camera>() == null)
            {
                for (int renderOffsetX = -1; renderOffsetX < 1; renderOffsetX++)
                {
                    for (int renderOffsetY = -1; renderOffsetY < 1; renderOffsetY++)
                    {
                        Vector2 thisChunk = new Vector2();
                        thisChunk.x = Mathf.RoundToInt((objectsToTrack[i].transform.position.x / 2) / chunkSize) + renderOffsetX;
                        thisChunk.y = Mathf.RoundToInt((objectsToTrack[i].transform.position.z / .6f) / chunkSize) + renderOffsetY;
                        chunksToMakeLive.Add(thisChunk);
                    }
                }
            }
            else
            {
                for (int renderOffsetX = -overRenderX; renderOffsetX < overRenderX; renderOffsetX++)
                {
                    for (int renderOffsetY = -overRenderY; renderOffsetY < overRenderY; renderOffsetY++)
                    {
                        Vector2 thisChunk = new Vector2();
                        thisChunk.x = Mathf.RoundToInt((objectsToTrack[i].transform.position.x/2) / chunkSize) + renderOffsetX;
                        thisChunk.y = Mathf.RoundToInt((objectsToTrack[i].transform.position.z/.6f) / chunkSize) + renderOffsetY;
                        chunksToMakeLive.Add(thisChunk);
                    }
                }
            }
        }




        for (int i = 0; i < liveChunks.Count; i++)
        {
            bool turnOff = true;
            for (int j = 0; j < chunksToMakeLive.Count; j++)
            {
                if(liveChunks[i].GetComponent<CMChunk>().x == chunksToMakeLive[j].x && liveChunks[i].GetComponent<CMChunk>().y == chunksToMakeLive[j].y)
                {
                    turnOff = false;
                    break;
                }
            }
            if(turnOff)
            {
                liveChunks[i].SetActive(false);
                liveChunks.RemoveAt(i);
                i--;
            }
        }
        liveChunks.Clear();

        for(int i = 0; i < chunksToMakeLive.Count; i++)
        {
            int chunkX = (int)chunksToMakeLive[i].x + chunkOffset ;
            int chunkY = (int)chunksToMakeLive[i].y + chunkOffset ;
            if (chunks[chunkX, chunkY] == null)
            {
                chunks[chunkX, chunkY] = new GameObject();
                chunks[chunkX, chunkY].name = "Chunk:" + (chunksToMakeLive[i].x ) + "," + (chunksToMakeLive[i].y );
                chunks[chunkX, chunkY].transform.parent = tileContainer.transform;
                CMChunk newChunk = chunks[chunkX, chunkY].AddComponent<CMChunk>();
                newChunk.x = (int)chunksToMakeLive[i].x ;
                newChunk.y = (int)chunksToMakeLive[i].y ;
                updatingTileList = true;
                for (int j = 0; j < chunkSize; j++)
                {
                    for (int k = 0; k < chunkSize; k++)
                    {
                        CMTile newTile = new CMTile();
                        newTile.position.x = k + ((chunksToMakeLive[i].x ) * chunkSize);
                        newTile.position.y = j + ((chunksToMakeLive[i].y ) * chunkSize);
                        newTile.parent = chunks[chunkX, chunkY].transform;
                        tilesToGenerate.Add(newTile);
                    }
                }
                updatingTileList = false;
            }
            if (!chunks[chunkX, chunkY].activeSelf)
            {
                chunks[chunkX, chunkY].SetActive(true);
            }
            liveChunks.Add(chunks[chunkX, chunkY]);
        }
        
        //added cause it's faster to do this than debug. still getting WAY to many tiles to generate for some reason.
        needToCreateCount = tilesToGenerate.Count;

        if(!processingTiles && tilesToGenerate.Count > 0)
        {
            StartCoroutine("ProcessTiles");
        }
    }

    bool processingTiles = false;
    bool updatingTileList = false;

    System.Collections.IEnumerator ProcessTiles()
    {
        processingTiles = true;
        int updateCount = 100;
        float lastTime = Time.time;
        int startLength = tilesToGenerate.Count;
        int counter = 0;
        while (tilesToGenerate.Count > 1)
        {
            if(counter >= startLength)
            {
                break;
            }
            //if slower than 60fps, decrease the number of tiles that generate per frame.
            deltaTime = Time.deltaTime;
            if (deltaTime > .015f && !firstLoop)
            {
                if (updateCount > 10)
                {
                    updateCount -= 5;
                }
            }

            //if faster than 100fps, increase the number of tiles that generate per frame.
            if(deltaTime < .014f)
            {
                updateCount += 5;
            }
            for (int i = 0; i < updateCount; i++)
            {
                if (tilesToGenerate.Count > 0)
                {
                    int x = (int)tilesToGenerate[0].position.x;
                    int y = (int)tilesToGenerate[0].position.y;
                    CMTile oldTile = tilesToGenerate[0];
                    GameObject newTile = GenerateTile(oldTile);
                    newTile.transform.parent = oldTile.parent;
                    tilesToGenerate.RemoveAt(0);
                }
                
            }
            if (firstLoop)
            {
                yield return new WaitForEndOfFrame();
            }
        }
        firstLoop = true;
        processingTiles = false;
    }

    GameObject GenerateTile(CMTile inputTile)
    {
        float x = Mathf.Floor(inputTile.position.x);
        float y = Mathf.Floor(inputTile.position.y);

        //commented out to increase generation speed.
        /*
        float total = 0;
        int count = 0;
        int averageDensity = 10;

        
        for (int j = -averageDensity; j <= averageDensity; j++)
        {
            for (int i = -averageDensity; i <= averageDensity; i++)
            {
                Random.seed = seed*(int)x*(int)y;
                total += (Random.Range((int)0, biomes.Count));
                count++;
            }
        }*/

        //int biome = Mathf.FloorToInt(total / count);
        int biome = Mathf.FloorToInt(
                    Mathf.PerlinNoise(
                        (((x+1000) * seed) / scale),
                        (((y+1000) * seed) / scale)
                        )
                        * biomes.Count);
        //biome = Mathf.RoundToInt(((float)biome*mergeStrength) + (float)(biome2*(1-mergeStrength)));
        GameObject tile;
        try {
            tile = biomes[Mathf.Clamp(biome,0,biomes.Count-1)].GenerateTile(seed,(int)x, (int)y);

            if (y % 2 == 0)
            {
                tile.transform.position = new Vector3(x * 2f, tile.transform.position.y, (y*.6f));
            }
            else
            { 
                tile.transform.position = new Vector3((x*2f)+1f, tile.transform.position.y, (y*.6f));
            }
            tile.name = x + "," + y;
            CMTile thisTile = new CMTile();
            thisTile.biome = biome;
            thisTile.position = new Vector2(x, y);
            thisTile.gameObject = tile;
            return tile;
        }
        catch(System.Exception ex)
        {
            Debug.Log("ERROR: " + biome + ", " + ex.Message);
            return null;
        }
    }

    void UpdateBounds()
    {
        //possibly a pointless function at the moment.
        Vector3 bottomLeftPosition = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, Camera.main.transform.position.y));
        Vector3 topRightPosition = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, Camera.main.pixelHeight, Camera.main.transform.position.y));

        screenBounds.left = Mathf.Floor(bottomLeftPosition.x);
        screenBounds.bottom = Mathf.Floor(bottomLeftPosition.z);
        screenBounds.right = Mathf.Floor(topRightPosition.x);
        screenBounds.top = Mathf.Floor(topRightPosition.z);
    }
}
public class CMBounds
{
    public float left;
    public float right;
    public float bottom;
    public float top;
}

public class CMTile
{
    public Vector2 position;
    public int biome;
    public CMBounds bounds;
    public GameObject gameObject;
    public Transform parent;
}