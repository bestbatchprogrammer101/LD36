﻿using UnityEngine;
using System.Collections;

public class BiomeController : MonoBehaviour {
    public GameObject baseMesh;
    public float tileHeightOffset = 0;
    public float objectHeightOffset = 0;
    public GameObject[] objectsToSpawn;
    public Vector2[] chanceRange;

    public GameObject GenerateTile(int seed, int x, int y)
    {
        GameObject baseReturn = Instantiate(baseMesh);
        baseReturn.transform.position = new Vector3(x, tileHeightOffset, y);
        for (int i = 0; i < objectsToSpawn.Length; i++)
        {
            Random.seed = seed * x * y;
            float rangeValue = Random.Range(0f, 1f);

            if (rangeValue > chanceRange[i].x && rangeValue < chanceRange[i].y)
            {
                GameObject child = Instantiate(objectsToSpawn[i]);
                child.transform.parent = baseReturn.transform;
                child.transform.localPosition = new Vector3(Random.Range(-.5f, .5f), objectHeightOffset, Random.Range(-.5f, .5f));
                child.transform.eulerAngles = new Vector3(child.transform.eulerAngles.x, Random.Range(-180, 180), child.transform.eulerAngles.z);
                break;
            }
        }

        return baseReturn;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
