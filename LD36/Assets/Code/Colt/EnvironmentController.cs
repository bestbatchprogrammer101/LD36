﻿using System.Collections;
using UnityEngine;

public class EnvironmentController : EntityController {

    public override void Awake() {
        base.Awake();
        MouseController.environment.Add(this);
        var models = gameObject.transform.FindChild("Models");
        var iSelectedChild = Random.Range(0, models.childCount);
        for(int i = 0; i < models.childCount; i++) {
            models.GetChild(i).gameObject.SetActive(i == iSelectedChild);
            models.gameObject.GetComponent<Rigidbody>().useGravity = false;
            models.gameObject.GetComponent<Rigidbody>().isKinematic = true;
        }
    }

    public void OnDisable() {
        MouseController.environment.Remove(this);
    }
}