﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class menuController : MonoBehaviour
{
    //Author InternetsLIVE
    public void StartGame()
    {
        SceneManager.LoadScene("Main");
    }
    public void Credits()
    {
        SceneManager.LoadScene("Credits");
    }
    public void ExitGame()
    {
        //only if executable
        Application.Quit();
    }

}
