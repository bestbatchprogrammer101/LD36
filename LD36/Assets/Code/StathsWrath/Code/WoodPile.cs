﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Created by StathsWrath

public class WoodPile : MonoBehaviour {
    private TimeOfDay dayTime;
    // Yes I just did this for the pun(logical too), because I can. Kappa
    private Stack<Fuel> pile = new Stack<Fuel>();
    private Season season;
    private Weather weather;
    private float woodDryingMod = 1f;

    public bool InRangeOfFire {
        get; protected set;
    }

    public void AddLogToPile(WoodFuel _log) {
        var _fuel = _log.myFuel;
        _fuel.dryingMod = woodDryingMod;

        pile.Push(_fuel);
    }

    public Fuel GrabFuel() {
        var log = pile.Pop();

        if(pile.Count == 0) {
            Destroy(gameObject);
            return null;
        }

        return log;
    }

    public void OnTriggerStay(Collider other) {
        // TODO: if other is fire in range of fire = true
    }

    private static float CalculateSeason(Season _season, float weatherMod) {
        switch(_season) {
        case Season.Spring:
            weatherMod = 1f;
            break;

        case Season.Summer:
            weatherMod = 1.2f;
            break;

        case Season.Autumn:
            weatherMod = .95f;
            break;

        case Season.Winter:
            weatherMod = .7f;
            break;

        default:
            break;
        }

        return weatherMod;
    }

    private static float CalculateTimeOfDay(TimeOfDay _dayTime, float weatherMod) {
        switch(_dayTime) {
        case TimeOfDay.Morning:
            weatherMod += .15f;
            break;

        case TimeOfDay.Midday:
            weatherMod += .25f;
            break;

        case TimeOfDay.Evening:
            weatherMod += -.1f;
            break;

        case TimeOfDay.Night:
            weatherMod += -.2f;
            break;

        default:
            break;
        }

        return weatherMod;
    }

    private float CalculateWeather(Weather _weather, float weatherMod) {
        switch(_weather) {
        case Weather.None:
        case Weather.Cloudy:
            weatherMod += 0;
            break;

        case Weather.Sun:
            weatherMod *= 1f;
            break;

        case Weather.Precipitation:
            weatherMod *= -1;
            break;

        default:
            break;
        }

        return weatherMod;
    }

    private float CalculateWoodDryingModifier(TimeOfDay _dayTime, Season _season, Weather _weather) {
        var weatherMod = 1f;

        weatherMod = CalculateSeason(_season, weatherMod);
        weatherMod = CalculateTimeOfDay(_dayTime, weatherMod);
        weatherMod = CalculateWeather(_weather, weatherMod);

        if(InRangeOfFire) {
            weatherMod += .5f;
        }

        return weatherMod;
    }

    private void OnDisable() {
        TimeManager.onSeasonChanged -= TimeManager_seasonChanged;
        WeatherManager.OnWeatherChanged -= WeatherManager_OnWeatherChanged;
    }

    private void OnEnable() {
        TimeManager.onSeasonChanged += TimeManager_seasonChanged;
        WeatherManager.OnWeatherChanged += WeatherManager_OnWeatherChanged;
    }

    private void TimeManager_seasonChanged(TimeOfDay _dayTime, Season _season) {
        dayTime = _dayTime;
        season = _season;

        woodDryingMod = CalculateWoodDryingModifier(dayTime, season, weather);

        foreach(var log in pile) {
            log.dryingMod = woodDryingMod;
        }
    }

    private void WeatherManager_OnWeatherChanged(Weather args) {
        weather = args;

        woodDryingMod = CalculateWoodDryingModifier(dayTime, season, weather);

        foreach(var log in pile) {
            log.dryingMod = woodDryingMod;
        }
    }
}