﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampfireFueled : MonoBehaviour {
    public GameObject fireEffect;
    public float initialFuel = 30;

    /// <summary>
    /// amount of fuel burned in a second
    /// </summary>
    public float rateOfBurn = 1;

    /// <summary>
    /// current fuel objects in the fire
    /// </summary>
    private List<Fuel> fuelOnFire = new List<Fuel>();
    /// <summary>
    /// actual fuel
    /// </summary>
    private float totalFuel;

    public void AddFuel(Fuel _fuel) {
        fuelOnFire.Add(_fuel);
    }

    public void StartTheFire() {
        fireEffect.SetActive(true);
        var fuel = ScriptableObject.CreateInstance<Fuel>();
        AddFuel(fuel);
        fuel.totalEnergy = initialFuel;
        StartCoroutine(Burn());
    }

    private IEnumerator Burn() {
        while(initialFuel > 0) {
            for(int i = fuelOnFire.Count - 1; i >= 0; i--) {
                totalFuel += fuelOnFire[i].Consume();
                if(fuelOnFire[i].IsConsumed)
                    fuelOnFire.Remove(fuelOnFire[i]);
            }

            totalFuel -= rateOfBurn;

            yield return new WaitForSeconds(1);
        }

        Debug.LogError("GG");
    }
}