﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(NavMeshAgent))]
public class WorkerMovement : MonoBehaviour {

    public float stoppingDistance = 2f;
    public Transform curTarget;
    private NavMeshAgent agent;
	
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

	// Update is called once per frame
	void Update ()
    {
        if(curTarget != null)
        {
            if (DistanceToTraget() <= stoppingDistance)
            {
                agent.Stop();
                curTarget = null;
            }
        }
	}

    public void MoveTo(Transform _target)
    {
        curTarget = _target;
        agent.SetDestination(_target.position);
        agent.Resume();
    }

    float DistanceToTraget()
    {
        return Vector3.Distance(transform.position, curTarget.position);
    }
}
