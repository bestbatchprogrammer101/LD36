﻿//Twitch Mareske
//Discord Maer

using UnityEngine;
using System.Collections;

public class ChatText : MonoBehaviour {

    public TextMesh myText;
    private Transform workTrans;

    public void SetText(string _text, Transform _workerTrans, Color _textColor)
    {
        myText.text = _text;
        myText.color = _textColor;
        workTrans = _workerTrans;
    }
}
