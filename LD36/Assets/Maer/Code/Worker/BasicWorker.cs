﻿//Twitch Mareske
//Discord Maer

using System.Collections;
using UnityEngine;

[RequireComponent(typeof(WorkerMovement))]
[RequireComponent(typeof(WorkerNeeds))]
public class BasicWorker : MonoBehaviour {

    #region Variables
    [Header("Referenzes")]
    public Ressource curRessource;
    public bool debug = false; //Checks if we want to display the Debug logs
    public int harvestDistance = 3;
    [Header("Harvesting")]
    public int harvestStrengh = 1;
    public bool isWaitingForWork = true; //Are we idling and waiting for work?

    //More strengh = fast tree chopping
    //How far we can stand away from the object we want to harvest

    //Current Ressource we want to harvest
    [HideInInspector]
    public WorkerMovement myMovement; //Referenz to Worker movement logic
    [HideInInspector]
    public WorkerNeeds myNeeds; //Referenz to Worker needs logic
    public TextMesh nameDisplay;
    [Header("Name")]
    public string wName;
    private Transform campfire; //campfire referenz to let the worker go back to the camp and wait there for new work

    //The name of the Worker
    //The name display, so that the players can see it
    #endregion Variables

    /// <summary>
    /// Let the worker get back to the camp to wait there and chill
    /// </summary>
    public void MoveToCamp() {
        myMovement.MoveTo(campfire);
    }

    void Start() {
        //Get referenzes
        myMovement = GetComponent<WorkerMovement>();
        myNeeds = GetComponent<WorkerNeeds>();
        myNeeds.myWorker = this;
        campfire = GameObject.FindGameObjectWithTag("Campfire").transform;

        //Adding worker to temp Manager
        TemperatureManager.me.AddWorker(this);

        //Gettin a new name for the worker
        wName = NameManager.me.GetRandomName();
        nameDisplay.text = wName;
        gameObject.name = wName;
    }

    #region Event

    Ressource lastResource;

    public void GetTick() {
        if(lastResource != null && lastResource != curRessource) {
            lastResource.StopHarvestEffects();
        }

        if(isWaitingForWork) {
            GetWork();
        } else {
            if(GetDistance() <= harvestDistance) {
                if(lastResource != curRessource) {
                    curRessource.StartHarvestEffects();
                }
                lastResource = curRessource;
                HarvestRessource();
            }
        }
    }

    void OnDisable() {
        TickManager.workTick -= GetTick;
    }

    void OnEnable() {
        TickManager.workTick += GetTick;
    }

    #endregion Event

    #region Work

    public void SetResourceToCollect(Ressource ressource) {
        curRessource = ressource;
        if(debug)
            Debug.Log("Got new Resource!");

        curRessource.curWorker = this;
        isWaitingForWork = false;
        MoveToRessource();
    }

    /// <summary>
    /// The brain of the Worker, trying to get work with each tickevent
    /// </summary>
    void GetWork() {
        var ressource = RessourceManager.me.GetRessource(this);

        if(ressource != null) {
            SetResourceToCollect(ressource);
        } else {
            isWaitingForWork = true;
            MoveToCamp();
        }
    }

    #endregion Work

    #region Ressource harvesting

    /// <summary>
    /// Feedback from the Ressource when its harvested
    /// </summary>
    public void ResourceCollectionComplete() {
        if(debug)
            Debug.Log("Finished Harvesting Resource!");
        curRessource = null;
        isWaitingForWork = true;
    }

    /// <summary>
    /// Calculating Distance beetween worker and ressource
    /// </summary>
    /// <returns></returns>
    float GetDistance() {
        return Vector3.Distance(this.transform.position, curRessource.transform.position);
    }

    /// <summary>
    /// Do your harveststrengh to the current ressource, to chop that Mothertrucker down
    /// </summary>
    private void HarvestRessource() {
        if(debug)
            Debug.Log("Harvesting Resource!");
        curRessource.harvest(this, harvestStrengh);
    }

    /// <summary>
    /// Move to the current ressource
    /// </summary>
    private void MoveToRessource() {
        if(debug)
            Debug.Log("Moving to Resource!");
        myMovement.MoveTo(curRessource.transform);
    }

    #endregion Ressource harvesting
}