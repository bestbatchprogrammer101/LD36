﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Ressource : MonoBehaviour {
    public int curHitpoints = 10;
    [Header("Referenzes")]
    public BasicWorker curWorker;
    public bool destroyObject = true;

    #region Drops
    public GameObject drop;
    public int dropMaxCount;
    public int dropMinCount;
    #endregion Drops

    //how hard is it to harvest this ressource?
    public int maxHitpoints = 10;

    #region Audio
    [Header("Audio")]
    public AudioClip harvestSound;
    public float harvestSoundVolume;
    GameObject currentPlayingSound;
    DateTime lastTimeAudioStarted;
    #endregion Audio

    [Header("Ressource")]
    private bool addedToList = false; //If this ressource got added to the list, to get harvested
                                      //What should we drop
    GameObject highlight;

    /// <summary>
    /// Work this ressource, happens every tick
    /// </summary>
    /// <param name="_strengh"></param>
    public void harvest(BasicWorker worker, int _strengh) {
        worker.transform.rotation = Quaternion.LookRotation(transform.position - worker.transform.position);
        if(DateTime.Now - lastTimeAudioStarted > TimeSpan.FromSeconds(harvestSound.length + 2)) {
            currentPlayingSound = SharedResources.PlaySoundEffect(harvestSound, harvestSoundVolume);
            lastTimeAudioStarted = DateTime.Now;
        }

        curHitpoints -= _strengh;

        if(curHitpoints <= 0) {
            lastTimeAudioStarted = DateTime.MinValue;
            HarvestActionComplete(worker);
            Destroy(currentPlayingSound);
        }
    }

    public void HarvestCompleteSpawnLoot(float time = 0) {
        curWorker.ResourceCollectionComplete();
        float delta = 0;
        var count = UnityEngine.Random.Range(dropMinCount, dropMaxCount + 1);
        for(int i = 0; i < count; i++) {
            var logDeltaPosition = transform.up * delta;
            logDeltaPosition = new Vector3(logDeltaPosition.x, 0, logDeltaPosition.z);
            Instantiate(drop, transform.position +
                new Vector3(0, 1, 0) + logDeltaPosition,
                 Quaternion.Euler(transform.rotation.eulerAngles
                 + new Vector3(0, UnityEngine.Random.Range(-30, 30), 0)));
            delta += drop.GetComponentInChildren<BoxCollider>().size.y
                * drop.transform.GetChild(0).localScale.y * .7f;
        }
        Invoke("DestroyGameObject", time);
    }

    /// <summary>
    /// If this object gets clicked with the mouse
    /// </summary>
    public void OnMouseDown() {
        Highlight();
    }

    internal virtual void StartHarvestEffects() {
    }

    internal virtual void StopHarvestEffects() {
    }

    protected virtual void HarvestActionComplete(BasicWorker worker) {
        RessourceManager.me.RemoveRessource(this);
    }

    void Awake() {
        curHitpoints = maxHitpoints;
        highlight = transform.FindChild("Highlight").gameObject;
    }

    void DestroyGameObject() {
        if(destroyObject)
            Destroy(gameObject);
    }

    /// <summary>
    /// Highlight the resource and add to resource manager
    /// </summary>
    void Highlight() {
        if(!addedToList) {
            highlight.SetActive(true);
            RessourceManager.me.AddRessource(this);
            addedToList = true;
        }
    }

    /// <summary>
    /// UnHighlight the resource and remove from resource manager
    /// </summary>
    void UnHighlight() {
        if(addedToList) {
            curWorker = null;
            highlight.SetActive(false);
            RessourceManager.me.RemoveRessource(this);
            addedToList = false;
        }
    }
}