﻿using System.Collections.Generic;
using UnityEngine;

public class RessourceManager : MonoBehaviour {
    public static RessourceManager me;
    public List<Ressource> ressourceList = new List<Ressource>();

    public void AddRessource(Ressource _ress) {
        ressourceList.Add(_ress);
    }

    public Ressource GetRessource(BasicWorker _worker) {
        float ressourceDist = Mathf.Infinity;
        Ressource tempRess = null;

        foreach(Ressource ress in ressourceList) {
            if(ress.curWorker == null) {
                float tempDist = Vector3.Distance(_worker.transform.position, ress.transform.position);
                if(tempDist < ressourceDist) {
                    tempRess = ress;
                    ressourceDist = tempDist;
                }
            }
        }

        return tempRess;
    }

    public void RemoveRessource(Ressource _ress) {
        ressourceList.Remove(_ress);
    }

    void Awake() {
        me = this;
    }
}