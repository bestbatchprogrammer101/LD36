﻿//Twitch Mareske
//Discord Maer

using UnityEngine;
using System.Collections;

public class LookAtCam : MonoBehaviour {

    private Transform cam;

	void Start ()
    {
        cam = Camera.main.transform;
	}
	
	void FixedUpdate ()
    {
        transform.LookAt(cam);
	}
}
