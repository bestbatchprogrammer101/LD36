﻿//Twitch Mareske
//Discord Maer

using UnityEngine;
using UnityEngine.UI;

public class TimeManagerUI : MonoBehaviour {

    public Text curDaytime;
    public Text curSeason;

    void OnEnable()
    {
        TimeManager.onDayTimeChanged += TimeManager_onDayTimeChanged;
        TimeManager.onSeasonChanged += TimeManager_onSeasonChanged;
    }

    void OnDisable()
    {
        TimeManager.onDayTimeChanged -= TimeManager_onDayTimeChanged;
        TimeManager.onSeasonChanged -= TimeManager_onSeasonChanged;
    }

    private void TimeManager_onSeasonChanged(TimeOfDay _dayTime, Season _season)
    {
        curDaytime.text = "" + _dayTime;
        curSeason.text = "" + _season;
    }

    private void TimeManager_onDayTimeChanged(TimeOfDay _dayTime, Season _season)
    {
        curDaytime.text = "" + _dayTime;
        curSeason.text = "" + _season;
    }
}
